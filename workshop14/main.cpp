#include <iostream>

using namespace std;

int add(int a, int b) {
    return a + b;
}

int fallDamage(int height, int health, int damage = 15) {
    cout << "health refernce " << &health << endl;
    return health - height * damage;
}

int main() {
    const auto start{std::chrono::steady_clock::now()};
    for (int i = 0; i < 1e9; i++) {
        cout << i << endl;
    }
    const auto end{std::chrono::steady_clock::now()};
    const std::chrono::duration<double> elapsed_seconds{end - start};
 
    std::cout << elapsed_seconds.count() << "s\n"; // Before C++20
    // std::cout << elapsed_seconds << '\n'; // C++20's chrono::duration operator<<
    return 0;
}