const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d');
document.querySelector('input[type="number"]').addEventListener('input', (e) => {
    draw(parseInt(e.target.value));
});



function draw(frequency) {
    ctx.beginPath();
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.closePath();
    ctx.fill();
    
    ctx.strokeStyle = "rgb(255,0,0)";
    ctx.moveTo(0, canvas.height / 2);
    ctx.lineTo(canvas.width, canvas.height / 2);
    ctx.stroke();
    ctx.moveTo(canvas.width / 2, 0);
    ctx.lineTo(canvas.width / 2, canvas.height);
    ctx.stroke();

    ctx.strokeStyle = 'rgba(0, 0, 0, 1)';
    ctx.moveTo(0, canvas.height / 2);
    let i = 0;
    while (i < canvas.width) {
        ctx.lineTo(i, canvas.height / 2 + 20 * Math.sin(i / frequency));
        i += 1;
    }
    ctx.stroke();
}

draw(20);
