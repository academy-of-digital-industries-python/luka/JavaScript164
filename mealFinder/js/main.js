import mealCard from "./components/mealCard.js";

const searchInput = document.querySelector('input[name="search"]');
const mealsDiv = document.querySelector('#meals')

searchInput.addEventListener('keydown', e => {
    // console.log(e.key);
    if (e.key.toLowerCase() == 'enter') {
        console.log(searchInput.value);

        fetch(`https://www.themealdb.com/api/json/v1/1/search.php?s=${searchInput.value}`)
            .then(response => response.json())
            .then(data => {
                data.meals.forEach(meal => {
                    mealsDiv.appendChild(mealCard(meal));
                })
            });
    }
})
