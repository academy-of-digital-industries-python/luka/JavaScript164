const params = new URLSearchParams(window.location.search);
const ingredientUl = document.querySelector('#ingredients');
const title = document.querySelector('h2');
const instructionsElement = document.querySelector('#instructions');
const mealId = params.get('id');

window.addEventListener('load', async () => {
    const response = await fetch(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=${mealId}`);
    const data = await response.json();

    const meal = data.meals[0];
    title.innerText = meal.strMeal;
    for (let i = 1; i <= 20; i++) {
        const ingredient = meal[`strIngredient${i}`];
        const measure = meal[`strMeasure${i}`];
        if (!ingredient)
            break;
        
        const li = document.createElement('li');
        li.innerText = `${ingredient} ${measure}`;
        ingredientUl.appendChild(li);
        console.log(ingredient, measure);
    }
    instructionsElement.innerText = meal.strInstructions;


    console.log(meal);
});