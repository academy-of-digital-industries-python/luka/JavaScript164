
function insert(array, element, index) {
    if (index > array.length || index < 0) {
        return array;
    }
    /*
    return [
        ...array.slice(0, index),
        element,
        ...array.slice(index)
    ]
    */
    // return array.slice(index).concat(element).concat(array.slice(0, index));

    const newArray = [];

    // console.log(firstNames.slice(0, 1), firstNames.slice(1));
    const leftHalf = array.slice(0, index);
    const rightHalf = array.slice(index);
    
    for (let i = 0; i < leftHalf.length; i++){
        newArray.push(leftHalf[i]);
    }
    newArray.push(element);
    for (let i = 0; i < rightHalf.length; i++) {
        newArray.push(rightHalf[i]);
    }
    
    return newArray;
}

let firstNames = [
    // elements
    "John",
    "Jane",
    "Jim",
    "Jessica",
];

let lastNames = ["Doe", "Dash", "Doe", "Doe"];
// const user = firstNames[0];
// console.log(user[user.length - 1]);
// console.log(firstNames[0]);
// console.log(firstNames[firstNames.length - 1]);

// Accessing elements in an array
// console.log(firstNames[0] + ' ' + lastNames[0]);
console.log(`${firstNames[0]} ${lastNames[0]}`);

// Iterating over an array
let firstName;
let lastName;
for (let i = 0; i < firstNames.length; i++) {
    firstName = firstNames[i];
    lastName = lastNames[i];
    // console.log(`${firstNames[i]} ${lastNames[i]}`);
    console.log(`${firstName} ${lastName}`);
}

for (let i = 0; i < firstNames.length; i++) {
    const firstName = firstNames[i];
    console.log(`Hello ${firstName}`);
    console.log(firstName[firstName.length - 1]);
}



// Changing elements in an array
console.log('\n');
firstNames[1] = "Josh";
console.log(firstNames);

// Adding elements to an array
firstNames.push("Jane");
firstNames.push("Jill");
firstNames.unshift("Jill");
console.log(firstNames);

// Removing elements from an array
firstNames.pop();
firstNames.shift();
console.log(firstNames);
console.log(firstNames.indexOf('Jim'));
console.log(firstNames.slice(1, 4));
console.log(firstNames);
console.log(firstNames.splice(1, 2));
console.log(firstNames);


console.log("\nInsertion");
console.log(firstNames);
firstNames.push("Josh");
firstNames.push("Josh2");
firstNames.push("Josh3");
firstNames.push("Josh4");

console.log(firstNames);
firstNames = insert(firstNames, "Jimmy", 4);
firstNames = insert(firstNames, "Jonathan", 4);
firstNames = insert(firstNames, "Jonathan", 0);
firstNames = insert(firstNames, "Jonathan", firstNames.length);
firstNames = insert(firstNames, "Gela", -1);


// function sortMyNumbers(a, b) {
//     return a - b;
// }



console.log(firstNames);
// console.log(firstNames[60000]);
// while (true) {
//     firstNames.pop();
//     console.log(firstNames);
// }

// sorting
const numbers = [9999, 1, -1, 100, -101, -1000, 5, -10001, 100];
// numbers.sort(sortMyNumbers);
numbers.sort((a, b) => a - b);
console.log(numbers);
numbers.sort((a, b) => b - a);
console.log(numbers);





