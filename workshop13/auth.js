
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.9.0/firebase-app.js";
import { 
    getAuth, 
    createUserWithEmailAndPassword, 
    signInWithEmailAndPassword,
    signOut
} from "https://www.gstatic.com/firebasejs/10.9.0/firebase-auth.js";


// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyAIGJveWZ2r0gN-kg5jbsAA_ZAE-7yju1Q",
    authDomain: "javascript-482fd.firebaseapp.com",
    projectId: "javascript-482fd",
    storageBucket: "javascript-482fd.appspot.com",
    messagingSenderId: "605098248158",
    appId: "1:605098248158:web:9857a77dd8907dba897f4f"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth();
console.log(auth);
const authForm = document.querySelector('#authenticationForm');
const email = authForm.querySelector('input[type="email"]');
const password = authForm.querySelector('input[type="password"]');
const hiddenContent = document.querySelector('#hiddenContent');

function onUserStateChanged() {
    if (auth.currentUser) {
        authForm.style.display = 'none';
        hiddenContent.style.display = 'block';
        document.querySelector('#username').innerText = auth.currentUser.email;
    } else {
        authForm.style.display = 'block';
        hiddenContent.style.display = 'none';
    }
}

auth.onAuthStateChanged((user) => {
    onUserStateChanged();
});

authForm.querySelector('button[id="signUp"]')
    .addEventListener('click', (e) => {
        e.preventDefault();
        e.stopPropagation();
        createUserWithEmailAndPassword(auth, email.value, password.value)
            .then((userCredential) => {
                const user = userCredential.user;
                console.log(user);
            })
            .catch((error) => {
                const errorCode = error.code;
                const errorMessage = error.message;
                console.log(errorMessage);
            });
    });

authForm.querySelector('button[id="signIn"]')
    .addEventListener('click', (e) => {
        e.preventDefault();
        e.stopPropagation();
        signInWithEmailAndPassword(auth, email.value, password.value)
            .then((userCredential) => {
                const user = userCredential.user;
                console.log(user);
            })
            .catch((error) => {
                const errorCode = error.code;
                const errorMessage = error.message;
                console.log(errorCode, errorMessage);
            });
    });


document.querySelector('button[id="signOut"]')
    .addEventListener('click', (e) => {
        e.preventDefault();
        e.stopPropagation();
        signOut(auth)
            .then(() => {
                console.log('Sign out');
            })
            .catch((error) => {
                console.log(error);
            });
    });

