
class Node {
    constructor(value) {
        this.value = value;
        this.next = null;
    }
}


class LinkedList {
    constructor() {
        this.head = null;
    }

    push(value) {
        if (this.head === null) {        
            this.head = new Node(value);
        } else {
            let current = this.head;
            while (current.next !== null) {
                current = current.next;
            }
            current.next = new Node(value);
        }
    }

    pop() {
        if (this.head === null) {
            return;
        }
        if (this.head.next === null) {
            this.head = null;
            return;
        }
        let current = this.head;
        while (current.next.next) {
            current = current.next;
        }
        current.next = null;
    }
}

// const node1 = new Node(5);

// const node2 = new Node(6);

// node1.next = node2;

// console.log(node1.next.value);

const list = new LinkedList();
list.push(5);
list.push(6);
list.push(7);
list.push(8);
list.pop();
list.pop();
list.pop();
list.pop();
list.pop();


let current = list.head;
while (current !== null) {
    console.log(current.value);
    current = current.next;
}






