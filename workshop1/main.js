console.log("Hello World!");
console.log("Hello John");

// this is a comment

// Arithmetic Operators

// addition
console.log("5 + 6 = ", 5 + 6);

// subtraction
console.log("5 - 6 = ", 5 - 6);

// multiplication
console.log("5 * 6 = ", 5 * 6);

// division
console.log("30 / 6 = ", 30 / 6);

// modulus
console.log("4 % 2 = ", 4 % 2);
console.log("5 % 2 = ", 5 % 2);

// multiple operations
console.log("5 + 6 * 10 = ", 5 + 6 * 10);
console.log("(5 + 6) * 10 = ", (5 + 6) * 10);

console.log(100, 1e2);
console.log(Math.pow(2, 5));
console.log(2 ** 5);


// special numbers
console.log("Infinity", Infinity);
console.log("-Infinity", -Infinity);
console.log("NaN", NaN);

// float
console.log(Math.floor(5.6));
console.log(Math.ceil(5.6));
console.log(Math.round(5.6));

console.log(0.1 + 0.2);
console.log((10 + 20) / 100);

// strings
console.log("This is a string!");
console.log("5 + 7")

// special characters
console.log("he said: \"hello mom\"");
console.log('he said: \'hello mom\'');

console.log('he said: "hello mom"');
console.log("he said: 'hello mom'");

console.log("hello\nworld");
console.log("hello\tworld");
console.log("hello\\world");


// string concatenation
console.log("hello" + " " + "world");
console.log("hello " + 5);


// type casting
console.log(5 + "5");
console.log("5" + 5);
console.log(5 * "2");
console.log("5" * 2);

console.log('5 + parseInt("5") = ', 5 + parseInt("5"));
console.log('5 + parseInt("5.5") = ', 5 + parseInt("5.5"));
console.log('5 + parseFloat("5.5")', 5 + parseFloat("5.5"));
console.log('5 + Number("5.5")', 5 + Number("5.5"));
console.log('5 + Number("5.5")', 5 + Number("5"));


// template strings
console.log(`5 + 6 = ${5 + 6}`);

