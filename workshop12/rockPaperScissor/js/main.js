const userChoiceDiv = document.querySelector('#userChoice');
const aiChoiceDiv = document.querySelector('#aiChoice');
const resultDiv = document.querySelector('#result');
let aiChoice;
let userChoice;

const results = {
    draw: 0,
    win: 0,
    lose: 0
}

const images = {
    0: './imgs/rock.png',
    1: './imgs/paper.webp',
    2: './imgs/scissors.png'
}
let result;

function play() {
    aiChoice = Math.floor(Math.random() * 3);
    displayResults();
}

for (const img of document.querySelectorAll('#choices>img')) {
    img.addEventListener('click', (e) => {
        userChoice = parseInt(e.target.dataset.value);
        play();
    })
}

function displayResults() {
    userChoiceDiv.src = images[userChoice];
    aiChoiceDiv.src = images[aiChoice];
    if (userChoice === 0 && aiChoice === 1) {
        result = 'You Lose';
    } else if (userChoice === 0 && aiChoice === 2) {
        result = 'You Win';
    } else if (userChoice === 0 && aiChoice === 0) {
        result = 'Draw';
    } else if (userChoice === 1 && aiChoice === 1) {
        result = 'Draw';
    } else if (userChoice === 1 && aiChoice === 2) {
        result = 'You Lose';
    } else if (userChoice === 1 && aiChoice === 0) {
        result = 'You Win';
    } else if (userChoice === 2 && aiChoice === 1) {
        result = 'You Win';
    } else if (userChoice === 2 && aiChoice === 2) {
        result = 'Draw';
    } else if (userChoice === 2 && aiChoice === 0) {
        result = 'You Lose';
    }
    resultDiv.innerHTML = (`
        <p>${result}</p>
        <ul>
            <li>Win: ${results.win}</li>
            <li>Draw: ${results.draw}</li>
            <li>Lose: ${results.lose}</li>
        </ul>
    `);


    switch (result) {
        case 'You Lose':
            results.lose += 1;
            break;
        case 'You Win':
            results.win += 1;
            document.querySelector('#winSound').play();
            break;
        default:
            results.draw += 1;
            break;
    }
}


