# JavaScript164


[Title](<JS Final Project Requirements.docx>)

### Assignments

- [How to Submit Assignments](https://www.youtube.com/watch?v=jXpT8eOzzCM)
- [Assignment 1](https://classroom.github.com/a/jE2HNiRp)
- [Assignment 2](https://classroom.github.com/a/WfcMRFJ5)
- [Assignment 3](https://classroom.github.com/a/CifzGFSv)
- [Assignment 4](https://classroom.github.com/a/qWSBkuix)
- [Assignment 5](https://classroom.github.com/a/qTVMWZiw)
- [Assignment 5-1](https://classroom.github.com/a/dmaiID1M)
- [Assignment 6](https://classroom.github.com/a/p2sHzLld)
- [Netflify Apps](https://javascript-mini-projects.netlify.app)
    - Day 1 - [Form Validator](https://javascript-mini-projects.netlify.app/formvalidator)
    - Day 2 - [Movie Booking](https://javascript-mini-projects.netlify.app/seatbooking)
    - Day 3 - [Exchange Rate Calculator](https://javascript-mini-projects.netlify.app/exchangerate)
    - Day 4 - [Menu Slider](https://javascript-mini-projects.netlify.app/menuslider)
    - Day 5 - [Hangman](https://javascript-mini-projects.netlify.app/hangman)
    - Day 6 - [Infinite Scrolling](https://javascript-mini-projects.netlify.app/infinitescrolling)
    - Day 7 - [Meal Finder](https://javascript-mini-projects.netlify.app/mealfinder)


### Resources

- [JS Book](https://1drv.ms/b/s!AmZJMrBsKhiOh8UDJDRDATZCy9M9VA?e=AYrQHP)
- [Algorithms](https://1drv.ms/b/s!AmZJMrBsKhiOguIqpsGg8IHNDLHyvA?e=dY2EVR)
- [OS (Operating System)](https://1drv.ms/b/s!AmZJMrBsKhiOhssEVmDi3IAII6_GsQ?e=PmCpn1)
