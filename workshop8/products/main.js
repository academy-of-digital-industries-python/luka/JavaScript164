const productsDiv = document.querySelector('#products');
const productLoading = document.querySelector('#products-loading');

function createProductDiv(product) {
    const productDiv = document.createElement('div');
    productDiv.innerHTML = `
        <div class="col">
            <div class="card">
                <img src="${product.thumbnail}"
                    width=400
                    height=400
                    class="card-img-top" 
                    alt="${product.title}">
                <h5 class="card-title">${product.title}</h5>
                <div class="card-body">
                    <p class="card-text">
                        ${product.description}
                    </p>
                </div>
            </div>
        </div>
    `;
    return productDiv;
}

window.addEventListener('load', () => {
    setTimeout(() => {
        fetch('https://dummyjson.com/products')
            .then(response => response.json())
            .then(data => {
                productLoading.hidden = true;
                for (const product of data.products) {
                    productsDiv.appendChild(createProductDiv(product));
                }
            });
    }, 200);

});


document.querySelector('#searchForm').addEventListener('submit', e => {
    e.preventDefault();
    e.stopPropagation();

    const searchValue = document.querySelector('#searchInput').value;
    if (!searchValue)
        return;
    productsDiv.innerHTML = '';
    productLoading.hidden = false;

    fetch(`https://dummyjson.com/products/search?q=${searchValue}`)
    .then(response => response.json())
    .then(data => {
        data.products.forEach(product => productsDiv.appendChild(createProductDiv(product)));
        if (!data.products.length)
            productsDiv.innerHTML = '<p>No search Results!</p>';
        productLoading.hidden = true;
    })

})