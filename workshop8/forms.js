const loginForm = document.querySelector('#login-form');
document.querySelector('a').addEventListener('click', (e) => {
    e.preventDefault();
    e.stopPropagation();
    console.log('Gotcha');
});


document.querySelector('a').addEventListener('click', (e) => {
    e.preventDefault();
    e.stopPropagation();
    console.log('Who\s there!');
});

document.querySelector('a').onclick = (e) => {
    e.preventDefault();
    e.stopPropagation();
    console.log('Gotcha from onclick');
};

document.querySelector('a').onclick = (e) => {
    e.preventDefault();
    e.stopPropagation();
    console.log('onclick 2nd');
};
document.querySelector('p').addEventListener('click', (e) => {
    console.log('Who called me?');
})


function warnUserOnEmptyInput(inputField, errorTextElement, error) {
    if (!inputField.value) {
        inputField.classList.add('border-danger');
        errorTextElement.innerText = error;
        errorTextElement.classList.add('text-danger');
    } else {
        inputField.classList.remove('border-danger');
        errorTextElement.innerText = '';
    }

}

loginForm.addEventListener(
    'submit', 
    (e) => {
        e.preventDefault();
        const emailInput = document.querySelector('#email-input');
        const passwordInput = document.querySelector('#password-input');

        warnUserOnEmptyInput(emailInput, emailInput.nextElementSibling, 'Cannot be empty');
        warnUserOnEmptyInput(passwordInput, passwordInput.nextElementSibling, 'Cannot be empty');

    }
);