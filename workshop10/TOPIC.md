
## Promises in JavaScript

### What is a Promise?

A promise is an object that represents the eventual completion (or failure) of an asynchronous operation, and its resulting value.

### Why do we need Promises?

Promises are used to handle asynchronous operations in JavaScript. They are easy to manage when dealing with multiple asynchronous operations where callbacks can create callback hell leading to unmanageable code.

### How do we use Promises?

A Promise is in one of these states:

- pending: initial state, neither fulfilled nor rejected.
- fulfilled: meaning that the operation completed successfully.
- rejected: meaning that the operation failed.

A Promise is created using the `Promise` constructor. A promise can be created from scratch using the `new Promise` syntax or by using the `Promise.resolve()` and `Promise.reject()` methods.

```javascript
let promise = new Promise(function(resolve, reject) {
  // executor (the producing code, "singer")
});
```

The `executor` is automatically executed when the promise is constructed. The `executor` should perform the async operation and then call `resolve` or `reject` to change the state of the promise object.

```javascript
let promise = new Promise(function(resolve, reject) {
  // the function is executed automatically when the promise is constructed
  // after 1 second signal that the job is done with the result "done"
  setTimeout(() => resolve("done"), 1000);
});
```

The `then` method is used to get the result of the promise. The `then` method takes two arguments, a callback for the success case and another for the failure case.

```javascript
promise.then(
  result => alert(result), // shows "done" after 1 second
  error => alert(error) // doesn't run
);
```

The `catch` method is used to handle the error. It is the same as calling `promise.then(null, errorHandlingFunction)`.

```javascript
promise.catch(alert); // shows "Error: Whoops!" if the promise is rejected
```

### Promise Chaining

The `then` method returns a new promise, which resolves to the value returned by the `then`'s callback. This allows for chaining of promises.

```javascript
new Promise(function(resolve, reject) {
  setTimeout(() => resolve(1), 1000);
}).then(function(result) {
  alert(result); // 1
  return result * 2;
}).then(function(result) {
  alert(result); // 2
  return result * 2;
}).then(function(result) {
  alert(result); // 4
});
```

### Promise.all

The `Promise.all` method takes an array of promises and returns a new promise. The new promise resolves when all of the promises in the array have resolved. If any of the promises reject, the new promise rejects.

```javascript
let promise1 = Promise.resolve(3);
let promise2 = 42;
let promise3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 100, 'foo');
});

Promise.all([promise1, promise2, promise3]).then((values) => {
  console.log(values); // [3, 42, "foo"]
});
```

### Promise
- [MDN Web Docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)
- [JavaScript.info](https://javascript.info/promise-basics)

