function createCar(name) {

    return {
        name: name,
        brand: 'Mercedes'
    };
}

const car = createCar('W124');
console.log(car);
console.log(createCar('3'));