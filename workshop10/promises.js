const promise = new Promise(
    (resolve, reject) => {
        // reject('Error message');

        setTimeout(() => {
            const number = Math.random() * 100;
            if (number < 50) {
                resolve(number);
            } else {
                reject(new Error(number));
            }
        }, 1000);
    }
);

promise
    .then(
        (number) => {
            console.log(number);
        },
    )
    .catch((error) => {
        console.log('Rejected: ', error);
    });

console.log(promise);