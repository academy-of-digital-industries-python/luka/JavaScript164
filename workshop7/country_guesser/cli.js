// Write a code for country guesser
// ფსევდო კოდი | Pseudo code
// 1. გვიჭრდება ქვეყანა: ქალაქების სია; ქულა (დავიწყოთ 0-დან); შეცდომები (0)
// 2. რენდომად არჩევს ქვეყანა: ქალაქის წყვილს (randomCountry)
// 3. მომხმარებელს შემოვატანინოთ შესაბამისი ქალაქი (guess)
// 4. randomCountry -ის ქალაქი იგივეა რაც guess
// 5. მაშინ ქულა გაიზარდოს 1-ით.
// 7. თუ არა და დავუწეროთ სწორი პასუხი; შეცდომები გაიზარდოს 1-ით;
// 8. თუ შეცდომების რაოდენობა3-ის ტოლია; დაბრუნდი 1 ხაზზე;
// 10. თუ ქულა არის 7 -ზე მეტი ან ტოლი; მოიგო და დავბრუნდეთ 1 ხაზზე;
// 11. გააგრძელე 2 ხაზიდან.


/**
 * countries = [
 *  {
 *      country: capital 
 *  },
 *  .
 *  .
 *  .
 * ]
 * score = 0
 * fails = 0
 * 
 * [loop]
 * randomCountry = random from countries
 * guess = take user input
 * if guess == randomCountry.capital:
 *      display success text
 *      score++
 * else:
 *      display fail text
 *      fails++
 *
 * if fails >= 3:
 *      display lose text
 *      stop [loop]
 * 
 * if success >= 7:
 *      display win text
 *      stop [loop]
 * [end loop]
 *  */
const countries = [];
let score = 0;
let fails = 0;
let randomCountry;
let guess;


function choice(arr) {
    const randomIndex = Math.floor(Math.random() * arr.length);

    return arr[randomIndex];
}

for (let i = 65; i < 75; i++) {
    countries.push({
        name: String.fromCharCode(i),
        capital: String.fromCharCode(i)
    });
}

// countries.forEach(country => console.log(country));

while (true) {
    randomCountry = choice(countries);
    guess = prompt(`What is the capital of ${randomCountry.name}?`);
    if (guess.toLowerCase() === randomCountry.capital.toLowerCase()) {
        console.log('Correct!');
        score++;
    } else {
        console.log(`Incorrect it's ${randomCountry.capital}`);
        fails++;
    }

    if (fails >= 3) {
        console.log('You lost the game');
        break;
    }
    if (score >= 7) {
        console.log('Congrats');
        break;
    }
}
