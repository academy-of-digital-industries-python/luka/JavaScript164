// DOM Elements
const countryElement = document.querySelector('#country');
const countryInputElement = document.querySelector('#country-input');
const displayElement = document.querySelector('#display');


const countries = [];
let score = 0;
let fails = 0;
let randomCountry;
let guess;


function choice(arr) {
    const randomIndex = Math.floor(Math.random() * arr.length);

    return arr[randomIndex];
}

function finishGame(text) {
    displayElement.innerText = text;
    countryInputElement.disabled = true;
}


function getRandomCountry() {
    randomCountry = choice(countries);
    countryElement.innerText = `What is the capital of ${randomCountry.name}?`;
}

for (let i = 65; i < 75; i++) {
    countries.push({
        name: String.fromCharCode(i),
        capital: String.fromCharCode(i)
    });
}


getRandomCountry();
countryInputElement.addEventListener('keydown', (e) => {
    if (e.key !== 'Enter') {
        return;
    }
    guess = countryInputElement.value;
    if (guess.toLowerCase() === randomCountry.capital.toLowerCase()) {
        displayElement.innerText = 'Correct!';
        score++;
    } else {
        displayElement.innerText = `Incorrect it's ${randomCountry.capital}`;
        fails++;
    }

    if (fails >= 3) {
        finishGame('You lost the game!');
    }
    if (score >= 7) {
        finishGame(`You Won ${score}!`);
    }

    getRandomCountry();
    countryInputElement.value = '';
});


