let cookies = 10000;

const multiplierElement = document.querySelector('#multiplier');
const cookieBtn = document.querySelector('#cookie');
const counterElement = document.querySelector('#counter');
const multiplierBtn = document.querySelector('#multiplier-button');
const multiplierCostElement = document.querySelector('#multiplier-cost');


cookieBtn.addEventListener('click', () => {
    cookies += parseInt(multiplierElement.innerText);
    counterElement.innerText = cookies;
});

multiplierBtn.addEventListener('click', () => {
    const cost = parseInt(multiplierCostElement.innerText);
    const multiplier = parseInt(multiplierElement.innerText);

    if (cookies >= cost) {
        cookies -= cost;
        counterElement.innerText = cookies;
        multiplierElement.innerText = multiplier * 2;
        multiplierCostElement.innerText = cost * 3;
    }
});

setInterval(() => {
cookieBtn.click();
    
}, 1000);

