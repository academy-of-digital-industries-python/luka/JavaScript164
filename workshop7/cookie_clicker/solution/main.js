let cookies = 10000000;
const counter = document.querySelector('#counter');
const cookieBtn = document.querySelector('#cookie');
const multiplier = document.querySelector('#multiplier');
const multiplierCost = document.querySelector('#multiplier-cost');
const multiplierBtn = document.querySelector('#multiplier-button');
const autoClicker = document.querySelector('#auto-clicker');
const autoClickerBtn = document.querySelector('#auto-clicker-button');
const autoClickerCost = document.querySelector('#auto-clicker-cost');
const autoClickerSpeed = document.querySelector('#auto-clicker-speed');
const autoClickerSpeedBtn = document.querySelector('#auto-clicker-speed-button');
const autoClickerSpeedCost = document.querySelector('#auto-clicker-speed-cost');
const intervalFunc = () => {
    const autoClickerValue = parseInt(autoClicker.innerText);
    cookies += autoClickerValue;
    counter.innerText = cookies;
};
let autoClickerInterval = setInterval(intervalFunc, 1000);

cookieBtn.addEventListener('click', () => {
    cookies += 1 * parseInt(multiplier.innerText);
    counter.innerText = cookies;
});

multiplierBtn.addEventListener('click', () => {
    const cost = parseInt(multiplierCost.innerText);
    const multiplierValue = parseInt(multiplier.innerText);
    if (cookies >= cost){
        multiplierCost.innerText = cost * 3;
        cookies -= cost;
        counter.innerText = cookies;
        multiplier.innerText = multiplierValue * 2;
    } else {
        alert('Not enough cookies!');
    }
});

autoClickerBtn.addEventListener('click', () => {
    const cost = parseInt(autoClickerCost.innerText);
    const autoClickerValue = parseInt(autoClicker.innerText);

    if (cookies >= cost) {
        autoClickerCost.innerText = cost * 2;
        cookies -= cost;
        autoClicker.innerText = autoClickerValue + 1;
    } else {
        alert('Not enough cookies!');
    }
});

autoClickerSpeedBtn.addEventListener('click', () => {
    const cost = parseInt(autoClickerSpeedCost.innerText);
    const speed = parseInt(autoClickerSpeed.innerText);

    if (cookies >= cost) {
        autoClickerSpeedCost.innerText = cost * 3;
        cookies -= cost;
        let newSpeed = 1000 + (speed * -100)
        if (newSpeed <= 100) {
            newSpeed = 100;
            autoClickerSpeedBtn.disabled = true;
            autoClickerSpeedBtn.innerText = 'Auto Clicker Speed Maxed out';
        }
        autoClickerSpeed.innerText = speed + 1;
        
        clearInterval(autoClickerInterval);
        autoClickerInterval = setInterval(intervalFunc, newSpeed);
        console.log(newSpeed);
    } else {
        alert('Not enough cookies!');
    }
})



