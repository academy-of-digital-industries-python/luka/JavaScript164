const list = document.querySelector('#main-list');
const todoInput = document.querySelector('#todo-input');

todoInput.addEventListener('keydown', (e) => {
    console.log(e);
    if (e.key === 'Enter') {
        const value = todoInput.value;
        if (value) {
            console.log(value);
            const li = document.createElement('p');
            const deleteButton = document.createElement('button');
            deleteButton.innerText = 'X';
            li.innerText = value;
            li.style.cursor = 'pointer';
            li.addEventListener('click', () => {
                if (li.style.textDecoration == 'line-through') {
                    li.style.textDecoration = 'none';
                } else {
                    li.style.textDecoration = 'line-through';
                }
            });
            
            deleteButton.addEventListener('click', () => {
                li.remove();
            });
            li.appendChild(deleteButton);
            list.appendChild(li);
            todoInput.value = '';
        }
    }
});
