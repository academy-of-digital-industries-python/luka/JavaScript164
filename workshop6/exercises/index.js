const myP = document.querySelector('#myId');
const p = document.createElement('p');
const list = Array.from(document.querySelector('#list').children);

myP.style.color = 'red';
myP.addEventListener('click', () => {
    myP.style.backgroundColor = 'blue';
});

p.innerText = 'New Paragraph';
document.body.appendChild(p);

console.log(list);
list.forEach((li) => {
    console.log(li);
    li.addEventListener('click', () => {
        if (li.style.backgroundColor === 'green') {
            li.style.backgroundColor = 'transparent';
        } else {
            li.style.backgroundColor = 'green';
        }
    });
})

