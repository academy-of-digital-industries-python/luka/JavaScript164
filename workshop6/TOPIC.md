### JavaScript DOM

The Document Object Model (DOM) is a programming interface for web documents. It represents the page so that programs can change the document structure, style, and content. The DOM represents the document as nodes and objects. That way, programming languages can connect to the page.

### DOM Nodes

The DOM represents the document as a tree of nodes. The most common node types are:

- Element nodes
- Text nodes
- Attribute nodes
- Comment nodes

### Accessing Elements

You can access elements by using the `document` object. The `document` object represents the entire HTML document. It is the root node of the document tree, and provides the primary access to the document's content.

```javascript
document.getElementById("myId");
document.getElementsByClassName("myClass");
document.getElementsByTagName("p");
document.querySelector(".myClass");
document.querySelectorAll(".myClass");
```

### Changing Elements

You can change the content of an HTML element by using the `innerHTML` property. The `innerHTML` property sets or returns the HTML content (inner HTML) of an element.

```javascript
document.getElementById("myId").innerHTML = "New content";
```

### Adding and Deleting Elements

You can add new elements to the document by using the `createElement()` method. The `createElement()` method creates an Element Node with the specified name.

```javascript
const newElement = document.createElement("p");
```

You can remove elements from the document by using the `removeChild()` method. The `removeChild()` method removes a specified child node of the specified element.

```javascript
const element = document.getElementById("myId");
element.removeChild(element.childNodes[0]);
```

### Event Listeners

You can add event listeners to elements by using the `addEventListener()` method. The `addEventListener()` method attaches an event handler to the specified element.

```javascript
document.getElementById("myId").addEventListener("click", myFunction);
```

### DOM Navigation

You can navigate the DOM by using the `parentNode`, `childNodes`, `firstChild`, `lastChild`, `nextSibling`, and `previousSibling` properties.

```javascript
const parent = document.getElementById("myId").parentNode;
const children = document.getElementById("myId").childNodes;
const firstChild = document.getElementById("myId").firstChild;
const lastChild = document.getElementById("myId").lastChild;

const nextSibling = document.getElementById("myId").nextSibling;
const previousSibling = document.getElementById("myId").previousSibling;
```

### DOM Style

You can change the style of elements by using the `style` property. The `style` property sets or returns the style attribute of an element.

```javascript
document.getElementById("myId").style.color = "red";
```


### Exercises

1. Change the color of the text in the paragraph with the id ```"myId"``` to red.
2. Add a new paragraph to the body of the document with the text ```"New paragraph"```.
3. Add a click event listener to the paragraph with the id ```"myId"``` that changes the color of the text to blue when clicked.
4. Create a list with 5 items and add a click event listener to each item that changes the color of the text to green when clicked.
5. ToDo App: Create a form with an input and a button. When the button is clicked, add a new item to a list with the text from the input. Add a click event listener to each item that removes the item from the list when clicked.