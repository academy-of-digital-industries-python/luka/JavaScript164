// const p = document.getElementById('my-paragraph');
const p = document.querySelector('#my-paragraph');
const paragraphs = Array.from(document.getElementsByTagName('p'));
// const evens = document.getElementsByClassName('even');
const evens = document.querySelectorAll('.even, .fiery');
const testDiv = document.querySelector('#test');

function getRandom(max) {
    return Math.round(Math.random() * max);
}

console.log(p.innerText);
console.log(paragraphs);
console.log(evens);

console.log(testDiv);
p.innerText = 'I\'m from JS!';
testDiv.innerHTML += '<p> I\'m from JS! </p>';
testDiv.style.color = '#fff';
testDiv.style.backgroundColor = '#000';
// testDiv.style.background = "url('./imgs/bck.jpg')";
// testDiv.style.backgroundSize = 'cover';
// document.body.style.background = "url('./imgs/bck.jpg')";

paragraphs[paragraphs.length - 1].remove();

// testDiv.removeChild(testDiv.querySelector('p'));
console.log(testDiv.childNodes);
console.log(testDiv.children);
testDiv.removeChild(testDiv.children[0]);

const newP = document.createElement('p');
newP.innerText = 'created from js!';
console.log(newP);
testDiv.appendChild(newP);

// testDiv.onclick = () => {
//     console.log('Hello');
// }
testDiv.addEventListener('click', () => {
    console.log('Hi there!');
    
    testDiv.style.backgroundColor = `rgb(${getRandom(255)}, ${getRandom(255)}, ${getRandom(255)})`;
    // testDiv.style.backgroundColor = 'red';
});

