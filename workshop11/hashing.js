const text = 'Attack zoo';

function scramble(word) {
    const letters = word.split('');
    const newLetters = letters.map(letter => {
        if (letter === ' ') {
            return letter;
        }

        const newLetterCode = letter.charCodeAt() + 1;
        return String.fromCharCode(newLetterCode);
    });
    // console.log(newLetters);
    return newLetters.join('');
}

function descramble(word) {
    const letters = word.split('');
    const newLetters = letters.map(letter => {
        if (letter === ' ') {
            return letter;
        }
        const newLetterCode = letter.charCodeAt() - 1;
        return String.fromCharCode(newLetterCode);
    });
    // console.log(newLetters);
    return newLetters.join('');
}

const scrambledText = scramble(text);
console.log(scrambledText);

console.log(descramble(scrambledText));
