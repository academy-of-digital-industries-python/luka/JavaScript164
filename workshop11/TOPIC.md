## Persistence in JavaScript

### LocalStorage
- **Local Storage** is a web storage that allows you to store data in the browser with no expiration date. This means the data will persist even after the browser is closed.
- **Local Storage** is a key-value store that stores data in the form of a string. This means that you can only store strings in local storage. If you want to store an object, you will need to convert it to a string using `JSON.stringify()` and then convert it back to an object using `JSON.parse()` when you retrieve it.
- **Local Storage** is a synchronous storage, which means that it blocks the main thread when you read or write data to it. This can cause performance issues if you are storing a large amount of data in local storage.

#### Example
```javascript
// Store data in local storage
localStorage.setItem('name', 'John Doe');

// Retrieve data from local storage
const name = localStorage.getItem('name');

// Remove data from local storage
localStorage.removeItem('name');
```

#### Example with Objects
```javascript
// Store object in local storage
const user = {
  name: 'John Doe',
  email: 'test@mail.com',
};
localStorage.setItem('user', JSON.stringify(user));

// Retrieve object from local storage
const user = JSON.parse(localStorage.getItem('user'));

// Remove object from local storage
localStorage.removeItem('user');
```

### SessionStorage
- **Session Storage** is a web storage that allows you to store data in the browser for the duration of the page session. This means that the data will be cleared when the page is closed or refreshed.
- **Session Storage** is similar to local storage in that it is a key-value store that stores data in the form of a string. You can only store strings in session storage, and you will need to convert objects to strings using `JSON.stringify()` and back to objects using `JSON.parse()` when you retrieve them.
- **Session Storage** is also a synchronous storage, which means that it blocks the main thread when you read or write data to it. This can cause performance issues if you are storing a large amount of data in session storage.

#### Example
```javascript
// Store data in session storage
sessionStorage.setItem('name', 'John Doe');

// Retrieve data from session storage
const name = sessionStorage.getItem('name');

// Remove data from session storage
sessionStorage.removeItem('name');
```

#### Example with Objects
```javascript
// Store object in session storage
const user = {
  name: 'John Doe',
  email: 'test@mail.com',
};
sessionStorage.setItem('user', JSON.stringify(user));

// Retrieve object from session storage
const user = JSON.parse(sessionStorage.getItem('user'));

// Remove object from session storage
sessionStorage.removeItem('user');
```

### Cookies
- **Cookies** are small pieces of data that are stored in the browser and sent to the server with every request. They are used to store user-specific information, such as login credentials, shopping cart items, and user preferences.
- **Cookies** are limited in size, typically around 4KB, and can only store strings. If you want to store an object, you will need to convert it to a string using `JSON.stringify()` and then convert it back to an object using `JSON.parse()` when you retrieve it.
- **Cookies** are sent with every request to the server, which can impact performance if you are storing a large amount of data in cookies. They are also vulnerable to security risks, such as cross-site scripting and cross-site request forgery.

#### Example
```javascript
// Store data in a cookie
document.cookie = 'name=John Doe';

// Retrieve data from a cookie
const cookies = document.cookie.split(';');
const name = cookies.find(cookie => cookie.trim().startsWith('name=')).split('=')[1];

// Remove data from a cookie
document.cookie = 'name=; expires=Thu, 01 Jan 2023 00:00:00 UTC';
```

## Request Types
- **GET**: The GET method requests a representation of the specified resource. Requests using GET should only retrieve data.
- **POST**: The POST method is used to submit an entity to the specified resource, often causing a change in state or side effects on the server.
- **PUT**: The PUT method replaces all current representations of the target resource with the request payload.
- **DELETE**: The DELETE method deletes the specified resource.

### Examples with Fetch API
```javascript
// GET request
fetch('https://jsonplaceholder.typicode.com/posts')
  .then(response => response.json())
  .then(data => console.log(data));

// POST request
fetch('https://jsonplaceholder.typicode.com/posts', {
  method: 'POST',
  body: JSON.stringify({
    title: 'foo',
    body: 'bar',
    userId: 1
  }),
  headers: {
    'Content-type': 'application/json; charset=UTF-8',
  },
})
  .then(response => response.json())
  .then(data => console.log(data));

// PUT request
fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'PUT',
  body: JSON.stringify({
    id: 1,
    title: 'foo',
    body: 'bar',
    userId: 1
  }),
  headers: {
    'Content-type': 'application/json; charset=UTF-8',
  },
})
  .then(response => response.json())
  .then(data => console.log(data));

// DELETE request
fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'DELETE',
})
  .then(response => response.json())
  .then(data => console.log(data));
```


### Challenges

#### Challenge 1
Rock, Paper, Scissors is a classic game that is often used to introduce the concept of conditional statements. In this challenge, you will create a simple Rock, Paper, Scissors game using JavaScript.
