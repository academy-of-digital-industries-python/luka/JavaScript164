    const main = document.querySelector('main');

const TILE_WIDTH = window.innerWidth / 300;
const TILE_HEIGHT = window.innerHeight / 300;


main.style.display = 'grid';
main.style.gridTemplateColumns = `repeat(${parseInt(window.innerWidth / TILE_WIDTH) + 1}, ${TILE_WIDTH}px)`;
main.style.gridTemplateRows = `repeat(${parseInt(window.innerHeight / TILE_HEIGHT)}, ${TILE_HEIGHT}px)`;


let div;

function randint(a, b) {
    return Math.floor(Math.random() * (b - a) + a); 
}
function getRandomColor() {
    return `rgb(${randint(0, 255)}, ${randint(0, 255)}, ${randint(0, 255)})`
}

const life = [];

for (let i = 0; i <= window.innerHeight; i += TILE_HEIGHT) {
    const row = [];
    life.push(row);
    for (let j = 0; j <= window.innerWidth; j += TILE_WIDTH) {
        div = document.createElement('div');

        div.style.width = `${TILE_WIDTH}px`;
        div.style.height = `${TILE_HEIGHT}px`;
        div.style.backgroundColor = getRandomColor();

        div.addEventListener('mouseover', (e) => {
            e.target.style.backgroundColor = getRandomColor();
        })
    
        main.appendChild(div);
        row.push(div);
    }
}

// setInterval(() => {
//     const divs = document.querySelectorAll('div');  
//     for (const div of divs) {
//         div.style.backgroundColor = getRandomColor();
//     }
// }, 10)

// for (let i = 0; i < 100; i++) {
//     const y = randint(1, life.length - 1);
//     const x = randint(1, life[0].length - 1);

//     life[x][y].style.backgroundColor = 'blue';
//     life[x + 1][y].style.backgroundColor = 'blue';
//     life[x - 1][y + 1].style.backgroundColor = 'blue';


// }

// life[5][5].style.backgroundColor = 'blue';
// life[4][5].style.backgroundColor = 'blue';
// life[6][5].style.backgroundColor = 'blue';




// function getPos(x, y) {
//     try {
//         return life[y][x];
//     } catch (e) {
//         return null;
//     }
// }

// setInterval(() => {
//     life.forEach((row, i) => {
//         row.forEach((cell, j) => {
//             let neighborCount = 0;
//             const neighbors = {
//                 'leftNeighbor': getPos(i, j - 1),
//                 'rightNeighbor': getPos(i, j + 1),
//                 'upperNeighbor': getPos(i - 1, j),
//                 'lowerNeighbor': getPos(i + 1, j),
//                 'upperLeftNeighbor': getPos(i - 1, j - 1),
//                 'upperRightNeighbor': getPos(i - 1, j + 1),
//                 'lowerLeftNeighbor': getPos(i + 1, j - 1),
//                 'lowerRightNeighbor': getPos(i + 1, j + 1)
//             };

//             for (const neighbor of Object.values(neighbors)) {
//                 if (neighbor && neighbor.style.backgroundColor) {
//                     neighborCount += 1;
//                 }
//             }
            
//             if (neighborCount < 2 || neighborCount > 3) {
//                 cell.style.backgroundColor = '';
//             } else if (neighborCount === 3 && !cell.style.backgroundColor) {
//                 cell.style.backgroundColor = 'blue';
//             }
//         })
//     })
    

// }, 1000);