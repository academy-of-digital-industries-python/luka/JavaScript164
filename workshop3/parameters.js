// Global scope
let x = 25;
const u = 60;

function f(x, y = 1, z = 3) {
    // default values 
    // never use default vales before positional parameters
    console.log(u);
    console.log(`f was called with x = ${x}, y = ${y}, z = ${z}!`)
    return x + y + z;
}

if (true) {
    let c = 60;
    var b = 80;
}
f();
f(1, 2);
// u += 100;
console.log(u);
// console.log(c);
console.log(b);