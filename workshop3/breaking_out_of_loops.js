let isAlive = true;
/*
while (true) {
    console.log("I'm still alive!");
    isAlive = prompt("Are you still alive?") === 'yes';
    if (!isAlive) {
        break;
    }
}
*/

for (let i = 1; i < 100; i++) {
    if (i % 3 !== 1) {
        continue;
    }
    console.log(i);
    if (!(i % 61)) {
        break;
    }
}