function g(x) {
    return x * 2;
}

function f(x) {
    return g(x + 1);
}

console.log(
    f(
        f(5)
    )
)