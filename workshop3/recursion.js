function f(x) {
    if (x <= 0) {
        return 0;
    }
    return x + f(x - 1);
}

console.log(f(10));