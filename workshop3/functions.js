function f(x) {
    // code block
    return x ** 2;
}

function greetUser(name) {
    // function definition
    let text = `Hello, ${name}!`;
    console.log(text);
    return text;
}
// console.log(f(5));
// console.log(f(6));
// console.log(f(7));
// console.log(x);

// function call / invoke
greetUser('John');
greetUser('Jane');
greetUser('Jack');
greetUser('Mary');
console.log(greetUser('Jane'));