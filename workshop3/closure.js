function f(x) {
    let result = x + 10;
    // return function(y) { // anonymous function
    //     return result + y;
    // }
    return (y) => result + y; // arrow function
}


const g = f(20);
console.log(g(10));
console.log(
    f(20)(10)
);