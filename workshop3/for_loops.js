// Global Scope

let i = 30;
// console.log(i); // undefined
for (let i = 0; i < 10; i++) {
    // scope of i is limited to the for loop
    {
        let i = "Hello World!"
        console.log(i);
    }
    console.log(i);
}

console.log(i);