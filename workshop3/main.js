console.log(Boolean("")); // false
console.log(Boolean(" ")); // true
console.log(Boolean("hello world")); // true
console.log(0.1 + 0.2 === 0.3);
console.log(5 == "5");
console.log(5 === "5");

console.log(100 > 101 || 100 < 101);
console.log(Math.max(1, 2, 3, 4, 5) || Math.min(1, 2, 3, 4, 5));

// Truthy and Falsy value
console.log(`Boolean(5) ${Boolean(5)}`);
console.log(Boolean(-5));
console.log(Boolean(0));

console.log(5 || 6);
console.log(5 && 6);
console.log(Math.max(0, 1, 2, 10, 11, 100) && Math.min(-100, -90, -1, -0.9, 0));

let content = "";

console.log(content || "No content");
// if (content !== "") {
//     console.log(content)
// } else {
//     console.log("No content");
// }


