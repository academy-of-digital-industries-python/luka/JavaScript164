
const cache = {};

function fib(n) {
    if (n === 0) {
        return 0;
    } else if (n === 1) {
        return 1;
    }
    if (cache[n]) {
        return cache[n];
    }
    cache[n] = fib(n - 1) + fib(n - 2);
    return cache[n];
}
console.log(fib(10));
console.log(cache);

