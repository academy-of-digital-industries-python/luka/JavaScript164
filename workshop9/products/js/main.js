import PI, * as t from './math.js';

try {
    console.log(t.add(7, 5));
} catch(e) {
    console.log(`Something happened ${e.message}`);
} finally {
    console.log('I will execute anyway!');
}

try {
    console.log(t.divide(5, 0));
} catch(e) {
    console.log(e);
}


console.log(5 / 0);
console.log(PI);