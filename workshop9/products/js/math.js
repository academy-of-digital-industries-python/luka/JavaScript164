export function add(a, b) {
    a.hello();
    return a + b;
}

export function subtract(a, b) {
    return a - b;
}

export function divide(a, b) {
    if (b === 0) {
        throw Error('Zero Division Error');
    }
    return a / b;
}

const PI = 3.14;


export default PI;