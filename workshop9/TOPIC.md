### Use Strict

The `use strict` directive was introduced in ECMAScript 5. It is used to enable strict mode, which is a restricted variant of JavaScript that disallows some of the more problematic features of the language.

```javascript
"use strict";
```

When strict mode is enabled, the following changes are made to the behavior of the code:

- Variables must be declared before they are used.
- Assigning a value to an undeclared variable is not allowed.
- Deleting a variable, function, or function argument is not allowed.
- Duplicating a parameter name is not allowed.
- Octal numeric literals are not allowed.
- The `this` keyword is undefined in functions that are not methods.

## Modules in JavaScript

### What is a Module?

A module is a reusable piece of code that encapsulates related functionality. Modules are used to organize code into smaller, more manageable pieces.

### Why do we need Modules?

Modules are used to organize code into smaller, more manageable pieces. They help to keep the code clean, maintainable, and reusable.

### How do we use Modules?

Modules are used to organize code into smaller, more manageable pieces. There are several ways to create and use modules in JavaScript.

```javascript
// module.js
export function add(a, b) {
  return a + b;
}

export function subtract(a, b) {
  return a - b;
}
```

```javascript
// main.js
import { add, subtract } from './module.js';

console.log(add(1, 2)); // 3
console.log(subtract(3, 2)); // 1
```

### Exporting default

The `export default` statement is used to export a single value from a module. The `export default` statement can be used to export a function, class, or object.

```javascript
// module.js
export default function add(a, b) {
  return a + b;
}
```

```javascript
// main.js
import add from './module.js';

console.log(add(1, 2)); // 3
```

### Importing all

The `import * as` statement is used to import all of the exports from a module as a single object.

```javascript

// module.js
export function add(a, b) {
  return a + b;
}

export function subtract(a, b) {
  return a - b;
}
```

```javascript
// main.js
import * as math from './module.js';

console.log(math.add(1, 2)); // 3
console.log(math.subtract(3, 2)); // 1
```


## Error Handling in JavaScript

### What is an Error?

An error is an object that represents an abnormal condition that has occurred in the code. Errors can be thrown, caught, and passed around in JavaScript.

### Types of Errors

There are several types of errors in JavaScript. The most common are:

- `SyntaxError`: An error in the syntax of the code.
- `ReferenceError`: An error when a non-existent variable is referenced.
- `TypeError`: An error when a value is not of the expected type.
- `RangeError`: An error when a value is not within the expected range.

### Error Handling

Error handling is the process of catching and handling errors in the code. In JavaScript, errors are handled using the `try...catch` statement.

```javascript
try {
  // code that may throw an error
} catch (error) {
  // code to handle the error
}
```

The `try` block contains the code that may throw an error. If an error is thrown, the `catch` block is executed. The `catch` block contains the code to handle the error.

```javascript
try {
  throw new Error('Something went wrong');
} catch (error) {
  console.log(error.message); // Something went wrong
}
```

The `finally` block is used to execute code after the `try` and `catch` blocks, regardless of whether an error was thrown.

```javascript
try {
  // code that may throw an error
} catch (error) {
  // code to handle the error
} finally {
  // code to execute after the try and catch blocks
}
```

### Throwing Errors

Errors can be thrown using the `throw` statement. The `throw` statement is used to create a new error object and throw it.

```javascript

function divide(a, b) {
  if (b === 0) {
    throw new Error('Cannot divide by zero');
  }
  return a / b;
}

try {
  console.log(divide(10, 0));
} catch (error) {
  console.log(error.message); // Cannot divide by zero
}
```
