let words = [];
let fails = [];
let guess = [];
let word = '';

const person = [
    document.querySelector('#head'),
    document.querySelector('#body'),
    document.querySelector('#hand-1'),
    document.querySelector('#hand-2'),
    document.querySelector('#foot-1'),
    document.querySelector('#foot-2')
]



function getRandomElement(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
}

function initGame() {
    document.querySelector('#guess').innerText = '';
    document.querySelector('#pressed').innerHTML = '';
    document.querySelector('#fails').innerText = '';
    person.forEach(part => {
        part.classList.remove('d-flex');
        part.hidden = 'true';
    });
    fails = [];
    guess = [];
    word = getRandomElement(words);
    Array.from(word).forEach(() => guess.push('-'));
}

function playGame() {
    initGame();

window.addEventListener('keypress', (e) => {
    const char = e.key;
    if (fails.includes(char) || guess.includes(char)) {
        alert('You already tried that key');
        return;
    }

    if (word.includes(char)) {
        Array.from(word).forEach(
            (char_, i) => {
            if (char === char_){
                guess[i] = char_;
            }
        });
    } else {
        fails.push(char);
        const part = person[fails.length - 1];
        part.classList.add('d-flex');

        part.hidden = false;
    }

    if (fails.length >= 7) {
        alert('You lost the game');
        playGame();
        return;
    }
    
    document.querySelector('#guess').innerText = guess.join('');
    document.querySelector('#pressed').innerHTML = `<h1>${char}</h1>`;
    document.querySelector('#fails').innerText = fails.join(', ');
});
}

console.log(words);
fetch('https://raw.githubusercontent.com/dwyl/english-words/master/words.txt')
.then(response => response.text())
.then(data => {
    words = words.concat(data.split('\n'));
    playGame(); 
});


