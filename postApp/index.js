fetch('https://jsonplaceholder.typicode.com/posts')
    .then((response) => response.json())
    .then((posts) => {

        const div = document.createElement('div');
        div.innerHTML = `<p>${posts[0].title}</p>`;
        div.firstChild.addEventListener('click', (e) => {
            const text = e.target.innerText;
            console.log(text);
            const input = document.createElement('input');
            div.innerHTML = '';
            input.value = text;
            input.style.width = '100%';
            input.addEventListener('keypress', (e) => {
                console.log(e.key)
                if (e.key.toLowerCase() === 'enter') {
                    div.innerHTML = `<p>${posts[0].title}</p>`;
                }
            })
            div.appendChild(input);
        });
        document.body.appendChild(div);
        console.log(posts)
    });

fetch('https://jsonplaceholder.typicode.com/posts/1')
    .then((response) => response.json())
    .then((posts) => console.log(posts));
fetch('https://jsonplaceholder.typicode.com/posts/2/comments')
    .then((response) => response.json())
    .then((posts) => console.log(posts));

fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    body: JSON.stringify({
        title: 'foo',
        body: 'bar',
        userId: 1,
    }),
    headers: {
        'Content-type': 'application/json; charset=UTF-8',
    },
    })
    .then((response) => response.json())
    .then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'DELETE',
        })
        .then((response) => {
            console.log(response.status);
            return response.json();
        })
        .then((json) => console.log(json));
