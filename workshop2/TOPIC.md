# Topics of Workshop 2

1. [Boolean](#boolean)
2. [Comparison](#comparison)
3. [Logical Operators](#logical-operators)
4. [Empty Values](#empty-values)
5. [Expressions](#expressions)
6. [Statements](#statements)
7. [Variable / Binding](#variable--binding)
8. [Naming variables](#naming-variables)
9. [The Environment](#the-environment)
10. [Functions](#functions)
11. [Control Flow](#control-flow)


#### Boolean
It is often useful to have a value that distinguishes between only two possibilities, like **“yes”** and **“no”** or **“on”** and **“off.”** For this purpose, JavaScript has a **Boolean** type, which has just two values, ```true``` and ```false```, which are written as those words.

#### Comparison
The most common way to obtain a Boolean value is to use the comparison operators. These are ```==``` (equal to), ```!=``` (not equal to), ```<``` (less than), ```>``` (greater than), ```<=``` (less than or equal to), and ```>=``` (greater than or equal to), all of which can be applied to two values. 

```js
console.log(3 > 2); // → true
console.log(3 < 2); // → false
```

#### Logical Operators
There are also some operations that can be applied to Boolean values themselves. JavaScript supports three logical operators: ```and```, ```or```, and ```not```. These can be used to “reason” about Booleans.

```js
console.log(true && false); // → false
console.log(true && true); // → true
console.log(false || true); // → true
console.log(false || false); // → false
console.log(!true); // → false
console.log(!false); // → true
```

#### Empty Values
There are two special values, written ```null``` and ```undefined```, that are used to denote the absence of a meaningful value. They are themselves values, but they carry no information.

```js
console.log(null);
console.log(undefined);
```


## Program Structure

---

#### Expressions

A fragment of code that produces a value is called an expression. Every value that is written literally (such as ```22``` or ```"psychoanalysis"```) is an expression. An expression between parentheses is also an expression, as is a binary operator applied to two expressions or a unary operator applied to one.

Some examples of expressions:
```js
5 + 7               // this expression produces a value
22                  // this expression produces a value
"psychoanalysis"    // this expression  produces a value
```

#### Statements
Statements are the instructions that make up a program and have *side effects* like changing the state of the machine or producing output. A program is a list of statements They are executed one after another, from top to bottom. A statement stands on its own and does something. 

Expressions did not change anything in the world, they just produce a value. so we cannot observe the result of an expression.

Some examples of statements:
```js
let x = 5; // this statement does something
console.log(x); // this statement does something
```


#### Variable / Binding
To catch and hold values, JavaScript provides a thing called a variable. A variable is a container that holds a value. You can think of a variable as a labeled box that stores a value. You can retrieve the value by naming the box.

```js
let x = 5;
let y = 10;
let z = x + y;
console.log(z); // → 15
```

```js
let firstName = "John";
let lastName = "Doe";

// using concatenation
console.log(firstName + " " + lastName); // → John Doe
// using template literals
console.log(`${firstName} ${lastName}`); // → John Doe
```


#### Naming variables
A variable name can include letters, digits, underscores, and dollar signs. It must not start with a digit. It is possible to use a digit at the start of a variable name, but this is considered a bad practice and should be avoided. 

Only the characters ```$``` and ```_``` are considered to be special in JavaScript. They are the only two characters that are allowed as the first character of a variable name.

You also should not use any of the reserved words as a variable name. [List of reserved words](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar#Keywords)

when naming variables, it is a good practice to use camelCase. Camel case is a naming convention in which the first letter of each word in a compound word is capitalized except for the first word. [More about camelCase](https://en.wikipedia.org/wiki/Camel_case)

```js
let name = "John";
let _name = "John";
let $name = "John";
let name1 = "John";
let 1name = "John"; // → SyntaxError: Invalid or unexpected token

// camelCase
let firstName = "John";
let lastName = "Doe";
let fullName = `${firstName} ${lastName}`;
console.log(fullName); // → John Doe
```

#### The Environment
The collection of variables and their values that exist at a given time is called the environment. When a program starts up, this environment is not empty. It always contains a number of standard variables. For example, functions and variables from the JavaScript standard library are available in all environments by default.

```js
console.log(Math.max(2, 4)); // → 4
console.log(Math.min(2, 4) + 100); // → 102
```

#### Functions
A function is a piece of program wrapped in a value. Such values can be applied in order to run the wrapped program. For example, in a browser environment, the standard library has functions for displaying text on the screen. 

A function returns a value. If it does not return a value, the value is ```undefined```.

```js
let firstName = prompt("What is your first name?");
console.log(`Hello, ${firstName}!`);
```

#### Control Flow
When your program contains more than one statement, the statements are executed as if they are a story, from top to bottom. 

```js
let x = Number(prompt("Pick a number"));
console.log(x ** 2);
```

This kind of execution is straight, but it is not always the best way to tell a story. When we want to create a branching story, we use conditional execution. 

```js
let number = Number(prompt("Enter a number"));
if (!Number.isNaN(number)) {
  console.log(`Your number is the square root of ${number * number}`);
}
```

*The ```Number.isNaN``` function is a standard JavaScript function that returns ```true``` only if the argument it is given is ```NaN```. The Number function happens to return ```NaN``` when you give it a string that doesn’t represent a valid number. Thus, the condition translates to “unless theNumber is not-a-number, do this.”*

```js
let theNumber = Number(prompt("Pick a number"));
if (theNumber < 10) {
  console.log("Small");
} else if (theNumber < 100) {
  console.log("Medium");
} else {
  console.log("Large");
}
```