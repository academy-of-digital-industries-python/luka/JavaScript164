let firstName = 'Josh';
let lastName = 'Doe';
let fullName = `${firstName} ${lastName}`;
let age = 15;
// camelCase
let isAdult = age >= 18;
// should not start with number
// only 2 symbols are allowed _ and $

console.log(22);
console.log(24 ** 2);

console.log(firstName);
console.log(age);

console.log(`I am ${firstName} and I am ${age} years old, isAdult = ${isAdult}`);

console.log(Math.round(7.5));
console.log(Math.ceil(7.1));
console.log(Math.floor(7.9));
let result = Math.max(-100, 100, 1, 2, 3, 4);
console.log(result);
console.log(Math.min(-100, 100, 1, 2, 3, 4));
