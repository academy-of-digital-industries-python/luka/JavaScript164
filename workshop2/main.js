
console.log("Hello world!");
console.log("Hello Gia!");
console.log("");
console.log(" ");


console.log(true);
console.log(false);

// Equality
console.log(`5 == 6 = ${5 == 6}`);
// Inequality
console.log(`5 != 6 = ${5 != 6}`);
// Greater than
console.log(`5 > 6 = ${5 > 6}`);
// Less than
console.log(`5 < 6 = ${5 < 6}`);
// Greater than or equal to
console.log(`5 >= 5 = ${5 >= 5}`);
// Less than or equal to
console.log(`5 <= 5 = ${5 <= 5}`);

// Be Careful
console.log(`5 == "5" = ${5 == "5"}`);
// Strict Equality
console.log(`5 === "5" = ${5 === "5"}`);
// Strict Inequality
console.log(`5 !== "5" = ${5 !== "5"}`);


// Logical Operators

// AND
console.log(`true && true = ${true && true}`);
console.log(`true && false = ${true && false}`);
console.log(`false && true = ${false && true}`);
console.log(`false && false = ${false && false}`);

// OR
console.log(`true || true = ${true || true}`);
console.log(`true || false = ${true || false}`);
console.log(`false || true = ${false || true}`);
console.log(`false || false = ${false || false}`);

console.log(`true && (false || false || true) ${true && (false || false || true)}`);
console.log(`false || ((true || true) && false) ${false || ((true || true) && false)}`);


// NOT
console.log(`!true = ${!true}`);
console.log(`!false = ${!false}`);


console.log(`(5 + 9) > 7 && 3 - 2 < 0 = ${(5 + 9) > 7 && 3 - 2 < 0}`);


// empty values
console.log(null);
console.log(undefined);
