console.log(parseInt(Math.random() * 250));
let firstName = prompt("What is your first name?");
let age = parseInt(prompt("What is your age?"));

// conditional statement 
if (age < 18 && age > 0) {
    // if body, code fragment, block
    console.log('You shall not pass!');
} else if (age <= 0){
    console.log('You are not born yet!');
} else {
    // else body, code fragment, block
    console.log(typeof age);
    console.log(`I am ${firstName} and I am ${age} years old`);
}


// case sensitivity
if (firstName.toLowerCase() === 'alice') {
    console.log('Hello Alice!');
}

