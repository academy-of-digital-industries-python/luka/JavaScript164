function forEach(arr, func) {
    for (const element of arr) {
        func(element);
    }
}

function power(num) {
    return num ** 2;
}

const numbers = [1, 2, 3, 4, 5];

forEach(numbers, console.log);
forEach(numbers, (num) => console.log(power(num)));
numbers.forEach(console.log);


console.log('\nMap');
console.log(numbers.map(power));

console.log('\nFilter');
console.log(numbers.filter((num) => num % 2 === 0));

console.log('\nReduce');
console.log(numbers.reduce((acc, num) => acc + num, 0));


console.log(
    numbers.reduce((acc, item) => {
        console.log(acc, item);
        return acc + item;
    }, 0)
);