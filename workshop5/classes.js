class Person { // PascalCase
    constructor(name, age) {
        this.name = name;
        this.age = age;
        this.friends = [];
    }

    addFriend(other) {
        this.friends.push(other);
        other.friends.push(this);
    }

    showFriends() {
        console.log(`${this.name} has: `)
        this.friends.forEach(console.log);
    }
}

class Student extends Person {
    constructor(name, age, gpa) {
        super(name, age);
        this.gpa = gpa;
    }
}

const george = new Person('George', 24);
const james = new Student('James', 35, 3.6);

console.log(george.name);
console.log(george.friends);
console.log(james.name);
console.log(james.friends);

// george.friends.push(james);
// james.friends.push(george);
george.addFriend(james);

james.showFriends();
george.showFriends();
