## js Objects
In JavaScript, an object is a standalone entity, with properties and type. Compare it with a cup, for example. A cup is an object, with properties. A cup has a color, a design, weight, a material it is made of, etc. The same way, JavaScript objects can have properties, which define their characteristics.

```javascript
const cup = {
  color: "blue",
  design: "swirl",
  weight: 0.5,
  material: "ceramic"
};
```

In the example above, `cup` is an object with properties.

### Object Properties
The properties of an object define the characteristics of the object. You access the properties of an object with a simple dot notation:

```javascript
const cup = {
  color: "blue",
  design: "swirl",
  weight: 0.5,
  material: "ceramic"
};

console.log(cup.color); // blue
console.log(cup.design); // swirl
console.log(cup.weight); // 0.5
console.log(cup.material); // ceramic
```

### Object Methods
Objects can also have methods. Methods are actions that can be performed on objects. Methods are stored in properties as function definitions.

```javascript
const cup = {
  color: "blue",
  design: "swirl",
  weight: 0.5,
  material: "ceramic",
  drink: function() {
    return "drinking";
  }
};

console.log(cup.drink()); // drinking
```

### Object keys
The `Object.keys()` method returns an array of a given object's own property names, in the same order as we get with a normal loop.

```javascript
const cup = {
  color: "blue",
  design: "swirl",
  weight: 0.5,
  material: "ceramic"
};

console.log(Object.keys(cup)); // ["color", "design", "weight", "material"]

for (let key in cup) {
  console.log(key); // color, design, weight, material
}

for (let key of Object.keys(cup)) {
  console.log(key); // color, design, weight, material
}
```

### Object values
The `Object.values()` method returns an array of a given object's own enumerable property values, in the same order as that provided by a for...in loop.

```javascript
const cup = {
  color: "blue",
  design: "swirl",
  weight: 0.5,
  material: "ceramic"
};

console.log(Object.values(cup)); // ["blue", "swirl", 0.5, "ceramic"]


for (let value of Object.values(cup)) {
  console.log(value); // blue, swirl, 0.5, ceramic
}

```

### Object entries
The `Object.entries()` method returns an array of a given object's own enumerable string-keyed property [key, value] pairs, in the same order as that provided by a for...in loop.

```javascript
const cup = {
  color: "blue",
  design: "swirl",
  weight: 0.5,
  material: "ceramic"
};

console.log(Object.entries(cup)); // [["color", "blue"], ["design", "swirl"], ["weight", 0.5], ["material", "ceramic"]]

for (let [key, value] of Object.entries(cup)) {
  console.log(key, value); // color blue, design swirl, weight 0.5, material ceramic
}
```


## js Classes
JavaScript classes, introduced in ECMAScript 6, are primarily syntactical sugar over JavaScript's existing prototype-based inheritance. The class syntax does
not introduce a new object-oriented inheritance model to JavaScript.

```javascript
class Car {
  constructor(brand) {
    this.carname = brand;
  }
  present() {
    return 'I have a ' + this.carname;
  }
}

mycar = new Car("Ford");
console.log(mycar.present()); // I have a Ford
```

### Class Inheritance
To create a class inheritance, use the `extends` keyword.

```javascript
class Car {
  constructor(brand) {
    this.carname = brand;
  }
  present() {
    return 'I have a ' + this.carname;
  }
}

class ElectricCar extends Car {
    constructor(brand, model) {
        super(brand);
        this.model = model;
    }
    show() {
        return this.present() + ', it is a ' + this.model;
    }
}

mycar = new ElectricCar("Tesla", "Model S");
console.log(mycar.show()); // I have a Tesla, it is a Model S
```

## Higher Order Functions
A higher-order function is a function that takes another function as an argument or returns a function as a result.

```javascript
function map(arr, fn) {
  const newArr = [];
  for (let i = 0; i < arr.length; i++) {
    newArr.push(fn(arr[i]));
  }
  return newArr;
}

const arr = [1, 2, 3, 4, 5];
const double = (x) => x * 2;
console.log(map(arr, double)); // [2, 4, 6, 8, 10]
```

### Callbacks
A callback function is a function passed into another function as an argument, which is then invoked inside the outer function to complete some kind of routine or action.

```javascript
function greeting(name) {
  alert('Hello ' + name);
}

function processUserInput(callback) {
  var name = prompt('Please enter your name.');
  callback(name);
}

processUserInput(greeting);
```

### Array Methods
JavaScript provides a number of methods that allow us to iterate over arrays. These methods are higher-order functions that accept callback functions as their arguments.

```javascript
const arr = [1, 2, 3, 4, 5];

arr.forEach((item, index) => {
  console.log(item, index);
});

arr.map((item, index) => {
  console.log(item, index);
});

arr.filter((item, index) => {
  console.log(item, index);
});

arr.reduce((acc, item, index) => {
  console.log(acc, item, index);
}, 0);
```

### Spread
The spread operator allows an expression to be expanded in places where multiple arguments (for function calls) or multiple elements (for array literals) are expected.

```javascript
const arr = [1, 2, 3, 4, 5];

console.log(...arr); // 1 2 3 4 5
console.log(Math.max(...arr)); // 5
```

### Rest
The rest parameter syntax allows a function to accept an indefinite number of arguments as an array.

```javascript
function sum(...args) {
  return args.reduce((acc, item) => acc + item, 0);
}

console.log(sum(1, 2, 3, 4, 5)); // 15
```

### Destructuring
The destructuring assignment syntax is a JavaScript expression that makes it possible to unpack values from arrays, or properties from objects, into distinct variables.

```javascript
const person = {
  name: "John",
  age: 30,
  city: "New York"
};

const { name, age, city } = person;

console.log(name, age, city); // John 30 New York
```

### Some exercises
1. Create a class `Person` with the properties `name` and `age`. Create a method `greet` that returns a greeting message.
2. Create a class `Student` that extends `Person` and has the property `grade`. Create a method `study` that returns a message.
3. Create a class `Teacher` that extends `Person` and has the property `subject`. Create a method `teach` that returns a message.
4. Create a class `School` that has the properties `students` and `teachers`. Create a method `addStudent` that adds a student to the school. Create a method `addTeacher` that adds a teacher to the school. Create a method `getStudents` that returns the students of the school. Create a method `getTeachers` that returns the teachers of the school.
5. Create a function `sum` that receives an indefinite number of arguments and returns the sum of all the arguments.
6. Create a function `max` that receives an array of numbers and returns the maximum number.
7. Create a function `min` that receives an array of numbers and returns the minimum number.
8. Create a function `average` that receives an array of numbers and returns the average of the numbers.
9. Create a function `isPalindrome` that receives a string and returns `true` if the string is a palindrome, `false` otherwise.
10. Create a function `isPrime` that receives a number and returns `true` if the number is prime, `false` otherwise.
