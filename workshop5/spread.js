// spread
function sum(...args) {
    console.log(args.reduce((a, b) => a + b, 0));
}
const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

console.log('\nSpread');
console.log(...numbers);
// console.log(1, 2, 3, 4, 5);
console.log(numbers);

console.log(
    [
        ...numbers.slice(5),
        ...numbers.slice(0, 5),
    ]
)

sum(...numbers, -5, -10);

const n = [18, 19, 33];
const [age1, age2, age3] = n;
console.log(age1, age2, age3);

const o = {
    name: 'John',
    age: 30,
    country: 'USA',
};

const { name, age, country } = o;
console.log(name, age, country);

