const cup = {
    color: 'red',
    design: 'plain',
    material: 'plastic',
    weight: 0.3,
};

console.log(cup.color);
console.log(cup.design);
console.log(cup.material);
console.log(cup.weight);

const car = {
    make: 'Toyota',
    model: 'Corolla',
    year: 2023,
    color: 'black',
    engine: {
        cylinders: 4,
        size: 2.5,
    },
    transmission: {
        type: 'automatic',
        gears: 6,
    },
    wheels: [
        {
            'size': 18,
            'type': 'alloy',
        },
        {
            'size': 18,
            'type': 'alloy',
        },
        {
            'size': 18,
            'type': 'alloy',
        },
        {
            'size': 18,
            'type': 'steel',
        }
    ]
}

// console.log(car.wheels[car.wheels.length - 1].type);

const persons = [
    {
        name: 'Josh Doe',
        'age': 25,
        height: 1.75,
        speak: () => {
            console.log('hi there!');
        },
        friends: [
            {
                name: 'John Doe',
                age: 25,
                height: 1.92,
            },
            {
                name: 'Jane Doe',
                age: 25,
                height: 1.70,
            }
        ]
    },
    {
        name: 'Jane Doe',
    }
]

/*
persons.forEach((person) => {
    let totalHeight = 0;

    // for (let i = 0; i < person.friends.length; i++) {
    //     console.log(person.friends[i].height);
    //     totalHeight += person.friends[i].height;
    // }
    
    person.friends.forEach((friend) => totalHeight += friend.height);
    
    console.log(`${person.name}'s friends avg height is: `);
    console.log(totalHeight / person.friends.length);
    
});
*/

const person = persons[0];

person.speak();