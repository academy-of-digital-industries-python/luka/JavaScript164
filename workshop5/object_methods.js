const cup = {
    color: "blue",
    design: "swirl",
    weight: 0.5,
    material: "ceramic"
};

console.log(Object.keys(cup));
for (const key in cup) {
    console.log(key, cup[key]);
}

for (const key of Object.keys(cup)) {
    console.log(key)
}

console.log('\nOnly Values')

// for (const value of Object.values(cup)) {
//     console.log(value);
// }

const values = Object.values(cup); 
// for (let i = 0; i < values.length; i++) {
//     console.log(values[i]);
// }

for (const value of values) {
    console.log(value);
}

console.log('\nEntries')
console.log(Object.entries(cup));
for (const [key, value] of Object.entries(cup)) {
    console.log(key, value);
}